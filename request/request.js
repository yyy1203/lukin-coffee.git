//公共路径
var baseUrl = "http://www.kangliuyong.com:10002";

if (process.env.NODE_ENV === "production") {
	//生产环境
	baseUrl = "http://www.kangliuyong.com:10002"
} else {
	//开发环境
	baseUrl = "http://www.kangliuyong.com:10002"
}
const request = (opt) => {
	opt = opt || {};
	opt.url = opt.url || '';
	opt.data = opt.data || {};
	//配置 公共的appkey
	opt.data.appkey = "U2FsdGVkX19WSQ59Cg+Fj9jNZPxRC5y0xB1iV06BeNA="
	opt.method = opt.method || 'GET';
	//设置 请求头
	opt.header = opt.header || {
		'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
	};
	// 获取 登录 token
	const token = uni.getStorageSync('token')
	//设置 token
	opt.data.tokenString = token


	opt.success = opt.success || function() {};
	//数据加载
	uni.showLoading({
		title: "正在加载...",
		icon: "none",
		mask: true
	})

	uni.request({
		//配置 路径
		url: baseUrl + opt.url,
		//配置 参数
		data: opt.data,
		//配置 请求方式
		method: opt.method,
		//配置 请求头
		header: opt.header,
		success: (res) => {
			//判断 是否 请求数据成功
			if (res.statusCode === 200) {
				//隐藏 加载
				uni.hideLoading()
			}
			if (res.data.code === 700) {
				//执行未登录的逻辑
			}
			//传递 返回的数据
			opt.success(res);
		},
		fail: () => {
			//请求失败的 提示
			uni.showToast({
				title: '请稍后重试'
			});
		}
	})
}

export default request;
