import Vue from 'vue'
import App from './App'
import request from "./request/request.js"
// 1.引入uView库
import uView from 'uview-ui';
Vue.use(uView);
Vue.prototype.$myRequest = request
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
